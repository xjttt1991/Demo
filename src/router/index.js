import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import MoreGoods from '@/components/MoreGoods'
import Detail from '@/components/GoodsDetail'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main
    },
    {
      path: '/detail/:id',
      name: 'goodsDetail',
      component: Detail
    },
    {
      path: '/moreGoods/:type',
      name: 'moreGoods',
      component: MoreGoods
    },
    {
      path: '/choice',
      name: 'choice',
      component: MoreGoods
    },
    {
      path: '/cart',
      name: 'cart',
      component: MoreGoods
    },
    {
      path: '/mine',
      name: 'mine',
      component: MoreGoods
    },
  ],
  mode: 'history'
})
